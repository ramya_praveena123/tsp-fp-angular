import { Component, PLATFORM_ID, Inject, OnInit } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  constructor(@Inject(PLATFORM_ID) private platformId: Object) {}

  ngOnInit(): void {
    if (isPlatformBrowser(this.platformId)) {
      const indicator = document.querySelector('.nav-indicator') as HTMLElement | null;
      const items = document.querySelectorAll('.nav-item');

      function handleIndicator(el: HTMLElement) {
        if (indicator) {
          indicator.style.width = `${el.offsetWidth}px`;
          indicator.style.left = `${el.offsetLeft}px`;
          indicator.style.backgroundColor = el.getAttribute('active-color') || '';
        }

        items.forEach(item => {
          item.classList.remove('is-active');
          item.removeAttribute('style');
        });

        el.classList.add('is-active');
        el.style.color = el.getAttribute('active-color') || '';
      }

      items.forEach((item, index) => {
        item.addEventListener('click', () => { handleIndicator(item as HTMLElement); });
        if (item.classList.contains('is-active')) {
          handleIndicator(item as HTMLElement);
        }
      });
    }
  }
}
