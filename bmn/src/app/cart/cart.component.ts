import { Component } from '@angular/core';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent {
  bodyActive: boolean = false;
  products = [
    {
      id: 1,
      name: 'Fish',
      image: 'fish.webp',
      price: 200
    },
    {
      id: 2,
      name: 'Chicken biryani',
      image: 'biryani.jpeg',
      price: 250
    },
    {
      id: 3,
      name: 'Char siu',
      image: 'char siu.jpeg',
      price: 300
    },
    {
      id: 4,
      name: 'paneer curry',
      image: 'curry.jpeg',
      price: 150
    },
    {
      id: 5,
      name: 'gulab jamun ',
      image: 'gulab jamun.jpg',
      price: 75
    },
    {
      id: 6,
      name: 'kebab ',
      image: 'kebab.jpg',
      price: 100
    },
    {
      id: 7,
      name: 'manchuria',
      image: 'manchuria.jpeg',
      price: 250
    },
    // Add other products here...
  ];
  cartItems: any[] = [];

  openShopping() {
    this.bodyActive = true;
  }

  closeShopping() {
    this.bodyActive = false;
  }

  addToCart(product: any) {
    const existingItem = this.cartItems.find(item => item.id === product.id);
    if (existingItem) {
      existingItem.quantity++;
    } else {
      this.cartItems.push({ ...product, quantity: 1 });
    }
  }

  removeFromCart(index: number) {
    this.cartItems.splice(index, 1);
  }

  decrementQuantity(item: any) {
    if (item.quantity > 1) {
      item.quantity--;
    }
  }

  incrementQuantity(item: any) {
    item.quantity++;
  }

  getTotalPrice() {
    return this.cartItems.reduce((total, item) => total + (item.price * item.quantity), 0);
  }
}
