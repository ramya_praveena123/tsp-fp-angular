import { NgModule } from '@angular/core';
import { BrowserModule, provideClientHydration } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { BmnService } from './bmn.service';
import { HeaderComponent } from './header/header.component'; // Import BmnService here
import { HttpClientModule, provideHttpClient } from '@angular/common/http';
import { HomeComponent } from './home/home.component';
import { ContactpageComponent } from './contactpage/contactpage.component';
import { IndComponent } from './ind/ind.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { RECAPTCHA_SETTINGS, RecaptchaFormsModule, RecaptchaModule, RecaptchaSettings } from 'ng-recaptcha';
import { CartComponent } from './cart/cart.component';
import { BookingComponent } from './booking/booking.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    HomeComponent,
    ContactpageComponent,
    IndComponent,
    AboutUsComponent,
    CartComponent,
    BookingComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
   FormsModule, HttpClientModule,
    ReactiveFormsModule,
    RecaptchaModule,
    RecaptchaFormsModule
  ],
  
  providers: 
  [ {
    provide: RECAPTCHA_SETTINGS,
    useValue: {
      siteKey: '6LdNNW8pAAAAAPQaQscbZniQjAztPO4Xry_KaSDw',
    } as RecaptchaSettings,
  },

    provideClientHydration(),
    provideHttpClient(), 
    BmnService],
  bootstrap: [AppComponent]
})
export class AppModule { }
