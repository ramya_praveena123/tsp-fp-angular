import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ContactpageComponent } from './contactpage/contactpage.component';

// import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { IndComponent } from './ind/ind.component';
import { CartComponent } from './cart/cart.component';
import { BookingComponent } from './booking/booking.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '', component:LoginComponent },
  { path: 'home', component: HomeComponent }, 
  { path: 'order', component: IndComponent }, 
  { path: 'booking', component: BookingComponent}, 


  { path: 'aboutus', component: AboutUsComponent}, 
  { path: 'contact', component: ContactpageComponent },// Route for the home page
  { path: 'cart', component: CartComponent },// Route for the home page

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }