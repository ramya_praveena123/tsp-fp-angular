import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css']
})
export class BookingComponent implements OnInit {
  currentDateTime = new Date();
  year = this.currentDateTime.getFullYear();
  month = (this.currentDateTime.getMonth() + 1).toString().padStart(2, '0'); // Ensure two digits
  date = (this.currentDateTime.getDate() + 1).toString().padStart(2, '0'); // Ensure two digits

  ngOnInit(): void {
    if (parseInt(this.date) < 10) {
      this.date = '0' + this.date;
    }
    if (parseInt(this.month) < 10) {
      this.month = '0' + this.month;
    }

    const dateTomorrow = `${this.year}-${this.month}-${this.date}`; // Correct usage of template literals
    
    const checkinElem = document.querySelector("#checkin-date") as HTMLInputElement;
    const checkoutElem = document.querySelector("#checkout-date") as HTMLInputElement;

    checkinElem.setAttribute("min", dateTomorrow);

    checkinElem.onchange = function () {
      checkoutElem.setAttribute("min", (this as HTMLInputElement).value);
    }
  }
}
