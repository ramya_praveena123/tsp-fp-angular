import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BmnService {
  isUserLoggedIn: boolean;
  loginStatus: any;

  constructor(private http: HttpClient) { 
    this.isUserLoggedIn = false;
    this.loginStatus = new Subject();
  }

  setIsUserLoggedIn() {
    this.isUserLoggedIn = true;
    this.loginStatus.next(true);
  }

  getIsUserLogged(): boolean {
    return this.isUserLoggedIn;
  }  

  getLoginStatus(): any {
    return this.loginStatus.asObservable();
  } 

  userLogin(emailId: any, password: any): any {
    return this.http.get('http://localhost:8085/userLogin/' + emailId + '/' + password).toPromise();
  }

  registerUser(userData: any): Promise<any> {
    return this.http.post('http://localhost:8085/addUser', userData).toPromise();
  }
}
