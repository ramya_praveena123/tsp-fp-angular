import { Component, OnInit, OnDestroy, ViewChild, ElementRef, Inject } from '@angular/core';
import ScrollReveal from 'scrollreveal';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [
    { provide: 'isBrowser', useValue: true } // Provide a value for isBrowser
  ]
})
export class HomeComponent implements OnInit, OnDestroy {
  constructor(@Inject('isBrowser') private isBrowser: boolean) {}

  @ViewChild('navMenu') navMenuRef!: ElementRef;
  private navMenu!: HTMLElement;

  ngOnInit(): void {
    if (this.isBrowser) {
      this.navMenu = this.navMenuRef.nativeElement;
      this.showMenu('nav-toggle', 'nav-menu');
      this.themeButton();
      this.scrollRevealAnimation();

      window.addEventListener('scroll', () => {
        this.scrollActive();
        this.scrollHeader();
        this.scrollTop();
      });
    }
  }

  ngOnDestroy(): void {
    // Cleanup code here
  }

  showMenu(toggleId: string, navId: string): void {
    const toggle = document.getElementById(toggleId);
    const nav = document.getElementById(navId);

    if (toggle && nav) {
      toggle.addEventListener('click', () => {
        nav.classList.toggle('show-menu');
      });
    }
  }

  linkAction(): void {
    if (this.navMenu) {
      this.navMenu.classList.remove('show-menu');
    }
  }

  scrollActive(): void {
    const sections = document.querySelectorAll('section[id]');

    sections.forEach(current => {
      const sectionHeight = (current as HTMLElement).offsetHeight;
      const sectionTop = (current as HTMLElement).offsetTop - 50;
      const sectionId = current.getAttribute('id');

      if (window.scrollY > sectionTop && window.scrollY <= sectionTop + sectionHeight) {
        const link = document.querySelector(`.nav__menu a[href*='${sectionId}']`);
        if (link) {
          link.classList.add('active-link');
        }
      } else {
        const link = document.querySelector(`.nav__menu a[href*='${sectionId}']`);
        if (link) {
          link.classList.remove('active-link');
        }
      }
    });
  }

  scrollHeader(): void {
    const header = document.getElementById('header');
    if (header && window.scrollY >= 200) {
      header.classList.add('scroll-header');
    } else if (header) {
      header.classList.remove('scroll-header');
    }
  }

  scrollTop(): void {
    const scrollTop = document.getElementById('scroll-top');
    if (scrollTop && window.scrollY >= 560) {
      scrollTop.classList.add('show-scroll');
    } else if (scrollTop) {
      scrollTop.classList.remove('show-scroll');
    }
  }

  themeButton(): void {
    const themeButton = document.getElementById('theme-button');
    const darkTheme = 'dark-theme';
    const iconTheme = 'bx-sun';

    const selectedTheme = localStorage.getItem('selected-theme');
    const selectedIcon = localStorage.getItem('selected-icon');

    const getCurrentTheme = () => document.body.classList.contains(darkTheme) ? 'dark' : 'light';
    const getCurrentIcon = () => themeButton?.classList.contains(iconTheme) ? 'bx-moon' : 'bx-sun';

    if (selectedTheme && themeButton) {
      document.body.classList[selectedTheme === 'dark' ? 'add' : 'remove'](darkTheme);
      themeButton.classList[selectedIcon === 'bx-moon' ? 'add' : 'remove'](iconTheme);
    }

    if (themeButton) {
      themeButton.addEventListener('click', () => {
        document.body.classList.toggle(darkTheme);
        themeButton.classList.toggle(iconTheme);
        localStorage.setItem('selected-theme', getCurrentTheme());
        localStorage.setItem('selected-icon', getCurrentIcon());
      });
    }
  }

  scrollRevealAnimation(): void {
    const ScrollReveal = require('scrollreveal');
    const sr = ScrollReveal.default({
      origin: 'top',
      distance: '30px',
      duration: 2000,
      reset: true
    });

    sr.reveal(`
      .home_data, .home_img,
      .about_data, .about_img,
      .services_content, .menu_content,
      .app_data, .app_img,
      .contact_data, .contact_button,
      .footer__content`, {
      interval: 200
    });
  }
}
