import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BmnService } from '../bmn.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm!: FormGroup;
  registerForm!: FormGroup;
  captchaResponse: string = '';

  constructor(private router: Router, private service: BmnService, private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
   email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });

    this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      location: ['', Validators.required],
      city: ['', Validators.required],
      pincode: ['', Validators.required],
      mobileNumber: ['', Validators.required]
    });
  }

  loginSubmit(loginFormForm: any) {
    if (this.loginForm.valid) {
      const email = this.loginForm.get('email')?.value;
      const password = this.loginForm.get('password')?.value;
      this.service.userLogin(email, password).then(() => {
        // If the request is successful, navigate to the home page
        this.router.navigate(['/home']);
      }).catch((error: any) => {
        // If the request fails, check the status code
        if (error.status === 401) {
          // Status code 401 indicates unauthorized access (invalid email/password)
          alert('Invalid email or password.');
        } else {
          // Handle other error cases
          console.error('Error logging in:', error);
          alert('Error logging in. Please try again.');
        }
      });
    } else {
      // If the form is invalid, display an error message or take appropriate action
      alert('Please enter a valid email and password.');
    }
  }
  

  registerSubmit(registerForm: any) {
    if (this.registerForm.valid) {
      this.service.registerUser(registerForm).then(response => {
        // Handle success response
        console.log('User registered successfully:', response);
        // Provide feedback to the user
        alert('Registration successful!');
        // Optionally, navigate the user to another page after successful registration
        // this.router.navigate(['/home']);
      }).catch(error => {
        // Handle error response
        console.error('Error registering user:', error);
        // Provide feedback to the user
        alert('Error registering user. Please try again.');
      });
    } else {
      // If the form is invalid, display an error message or take appropriate action
      alert('Please fill in all fields.');
    }
  }
  handleCaptchaResponse(captchaResponse: string) {
    this.captchaResponse = captchaResponse;
  }
}
